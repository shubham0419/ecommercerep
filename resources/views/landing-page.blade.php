<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel Ecommerce Example</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Styles -->
      <link rel="stylesheet" href="{{ asset('/public/css/app.css') }}">
      <link rel="stylesheet" href="{{ asset('/public/css/responsive.css') }}">
   </head>
   <body>
      <div id="app">
         <header>
            <div class="top-nav container">
               <div class="top-nav-left">
                  <div class="logo" style="width:100px; height: 40px;"><a href="{{url('/')}}"><img src="{{asset('/public/storage/users/ecommerce.png')}}" ></a></div>
               </div>

               <div class="top-nav-right">
                  @include('partials.menus.main-right')
               </div>
            </div>
            <!-- end top-nav -->
         </header>
         <div class="featured-section">
            <div class="container">
               
               <div class="might-like-section">
                  <div class="container">
                     <div class="might-like-grid">
                        @forelse ($products as $product)
                        <a href="{{ route('shop.show', $product->slug) }}" class="might-like-product">
                           <img src="{{ productImage($product->image) }}" alt="product">
                           <div class="might-like-product-name">{{ $product->name }}</div>
                           <div class="might-like-product-price">{{ $product->presentPrice() }}</div>
                             @if ($product->quantity > 0)
                            <form action="{{ route('cart.store', $product) }}" method="POST">
                               {{ csrf_field() }}  
                              <button type="submit">
                                 <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              </button>
                           </form>
                           @endif
                        </a>
                        @endforeach
                     </div>
                  </div>
               </div>
               <div class="text-center button-container">
                  <a href="{{ route('shop.index') }}" class="button">View more products</a>
               </div>
            </div>
            <!-- end container -->
         </div>
         <!-- end featured-section -->
         @include('partials.footer')
      </div>
      <!-- end #app -->
      <script src="public/js/app.js"></script>
   </body>
</html>