<header>
    <div class="top-nav container">
      <div class="top-nav-left">
          <div class="logo" style="width:100px; height: 40px;"><a href="{{url('/')}}"><img src="{{asset('/public/storage/users/ecommerce.png')}}" ></a></div>
      
      </div>
      <div class="top-nav-right">
          @if (! (request()->is('checkout') || request()->is('guestCheckout')))
          @include('partials.menus.main-right')
          @endif
      </div>
    </div> <!-- end top-nav -->
</header>
