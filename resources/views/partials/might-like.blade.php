<div class="might-like-section">
    <div class="container">

        <div class="might-like-grid">
            @foreach ($mightAlsoLike as $product)
                <a href="{{ route('shop.show', $product->slug) }}" class="might-like-product">
                    <img src="{{ productImage($product->image) }}" alt="product">
                    <div class="might-like-product-name">{{ $product->name }}</div>
                    <div class="might-like-product-price">{{ $product->presentPrice() }}</div>
                 @if ($product->quantity > 0)
                            <form action="{{ route('cart.store', $product) }}" method="POST">
                               {{ csrf_field() }}  
                              <button type="submit">
                                 <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              </button>
                           </form>
                           @endif
                </a>

            @endforeach

        </div>
    </div>
</div>
