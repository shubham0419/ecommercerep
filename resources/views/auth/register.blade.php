@extends('layout')
@section('title', 'Sign Up for an Account')
@section('content')
<div class="container">
   <div class="auth-pages">
      <div>
         <h2>Create Account</h2>
         <div class="spacer"></div>
         <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group row @if ($errors->has('name')) has-error @endif">
               <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
               @if ($errors->has('name')) 
               <p class="help-block">{{ $errors->first('name') }}</p>
               @endif
            </div>
            <div class="form-group row @if ($errors->has('email')) has-error @endif">
               <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" >
               @if ($errors->has('name')) 
               <p class="help-block">{{ $errors->first('name') }}</p>
               @endif
            </div>
            <div class="form-group row @if ($errors->has('password_confirmation')) has-error @endif">
               <input id="password" type="password" class="form-control" name="password" placeholder="Password" placeholder="Password" >
               @if ($errors->has('password')) 
               <p class="help-block">{{ $errors->first('password') }}</p>
               @endif
            </div>
            <div class="form-group row @if ($errors->has('password_confirmation')) has-error @endif">
               <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"
                  >
               @if ($errors->has('password-confirm')) 
               <p class="help-block">{{ $errors->first('password-confirm') }}</p>
               @endif
            </div>
            <div class="login-container">
               <button type="submit" class="auth-button">Create Account</button>
               <div class="already-have-container">
                  <p><strong>Already have an account?</strong></p>
                  <a href="{{ route('login') }}">Login</a>
               </div>
            </div>
         </form>
      </div>
   </div>
   <!-- end auth-pages -->
</div>
@endsection