@extends('layout')

@section('title', 'Login')

@section('content')
<div class="container">
    @if (session()->has('error_message'))
   <div class="alert alert-danger">
      {{ session()->get('error_message') }}
      
   </div>
   @endif
    <div class="auth-pages">
        <div class="auth-left">
           
            <h2>Login</h2>
            <div class="spacer"></div>

            <form action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}


            <div class="form-group row @if ($errors->has('email')) has-error @endif">
                <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email">
                 @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
            </div>

            <div class="form-group row @if ($errors->has('password')) has-error @endif">
                <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password">
                 @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
            </div>
                <div class="login-container">
                    <button type="submit" class="auth-button">Login</button>
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>

                <div class="spacer"></div>

                <a href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>

            </form>
        </div>

        
    </div>
</div>
@endsection
