@extends('layout')
@section('title', 'Products')
@section('extra-css')
<link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection
@section('content')
@component('components.breadcrumbs')
<a href="{{url('/')}}">Home</a>
<i class="fa fa-chevron-right breadcrumb-separator"></i>
<span>Shop</span>
@endcomponent
<div class="container">
   @if (session()->has('success_message'))
   <div class="alert alert-success">
      {{ session()->get('success_message') }}
   </div>
   @endif
   @if(count($errors) > 0)
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
</div>
<div class="products-section container">
   <div class="sidebar">
      <h3>By Category</h3>
      <ul>
         @foreach ($categories as $category)
         <li class="{{ setActiveCategory($category->slug) }}"><a href="{{ route('shop.index', ['category' => $category->slug]) }}">{{ $category->name }}</a></li>
         @endforeach
      </ul>
   </div>
   <!-- end sidebar -->
   <div>
      <div class="products-header">
         <h1 class="stylish-heading">{{ $categoryName }}</h1>
        
      </div>
      <div class="might-like-section">
         <div class="container">
            <div class="might-like-grid">
               @forelse ($products as $product)
               <a href="{{ route('shop.show', $product->slug) }}" class="might-like-product">
                  <img src="{{ productImage($product->image) }}" alt="product">
                  <div class="might-like-product-name">{{ $product->name }}</div>
                     <div class="might-like-product-price">{{ $product->presentPrice() }}</div>
                    <!--  <button type="submit"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>   -->
                     @if ($product->quantity > 0)
                      <form action="{{ route('cart.store', $product) }}" method="POST">
                         {{ csrf_field() }}  
                        <button type="submit">
                           <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </button>
                     </form>
                     @endif
               </a>
               @endforeach
            </div>
         </div>

      </div>
      <div class="spacer"></div>
      {{ $products->appends(request()->input())->links() }}
   </div>
</div>
@endsection
@section('extra-js')
<!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
<script src="{{ asset('js/algolia.js') }}"></script>
@endsection